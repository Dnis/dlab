### 1.4.3.2
#### Bugfixes
* Fixed a bug where a cache hit was not recognized

### 1.4.3.1
#### Bugfixes
* Fixed a bug where the attribute `linewidth` was wrongly given to `FacetGrid`

### 1.4.3.0
#### Features
* A hue order can now be added
* ECDF plots now have different line styles

### 1.4.2.0
#### Features
* Added option for dropping NA values in the processing step
* Enabled log-scale for ECDF plot

#### Bugfixes
* Fix: Plots would overlap if saved consecutively
* Fix: Box plot grid would not be decorated

### 1.4.1.0
#### Features
* Added more descriptive output, which is especially helpful when running a batch of 
experiments
* Added an option to not terminate when an experiment fails. This option is now turned on
by default. Instead of terminating the number of failed experiments are printed out and 
the traceback can be inspected in the log or in the database.
* Two new plots are available: procedural box plots and procedural ecdf plots. They are capable 
of running a function on each instance with the values of all methods available. Can 
be useful for comparing relative values between methods.

#### Changes
* the dependency sacred won't output errors anymore

#### Bugfixes
* Fix: Boxplot could plot horizontally, although they should plot vertically
* Fix: an assertion falsely detected an exception

### 1.4.0.1
####  Bugfixes
* Fix: Optional dependency TinyDB was not correctly handled and an error was thrown if TinyDB was not installed
* Fix: fixed bug which would wrongly trigger an assertion

### 1.4.0.0
Version 1.4.0.0 is not compatible with 1.3.x! Hence, some code changes must be done in order to switch to 1.4.x.
#### Features
* Added possibility to run experiments from collection of dicts, individual dicts or directories of jsons.
* Added function which allows the user to choose the experiments over a CLI multi-select menu (if a directory or collection of dicts is provided).
* For each experiment-version a cache is now generated, which reduces the build up times for caches.
* The lab can be forced to use the cache instead of querying a database. This can be useful if computations are written into a database while evaluations are being made. Otherwise, the cache would be rebuilt each time an evaluation would be made.
* TinyDB is now supported. 
* Pymongo and tinydb are now optional dependencies.

#### Changes
* Removed DBAccess singleton.
* Now plots must be generated via a lab instance since they require connection information from the lab.
* Some plot functions are moved further down in the interface hierarchy.
* Now databases must be registered with a lab instances. Previously, a lab object had to be provided with MongoDB access information when constructing said object.

### 1.3.1.1
#### Bugfixes
* Fix: hide_ylabel would not work for box plots.
* Refactored box plots.
* Fix: Procedural columns would not be added to global_comparison_database_collector.

### 1.3.1.0
#### Features
* Added methods to enable/disable sharing of x/y axis.
* Added several evaluation tables including MeanTable, MedianTable, MinTable, and MaxTable.

#### Bugfixes
* Changed an error message to better reflect the error.
* Fixed an error where a variable was not refactored properly.